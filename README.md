# REST-api server

This is part of preservation system for High Performance Computing Systems in Baden-Wuerttemberg. Was developed along CiTAR project as one of part of it.

Script for push information about job-environment to server for creation container. Get all information about job from HPC automatically, parsing output from "checkjob" command.

## Getting Started

These instructions will give you information HOWTO setup and run application. How to setup python environment.

### Prerequisites

```
Give examples
```

### Installing

A step by step series of examples that tell you how to get a development env running


## Using

```
$python parse_job.py {job_id}
```

## Authors

* **Kyryll Udod** - *Developer* - [kirilludod](kirilludod)

## License

This project is licensed under the Apache 2.0 License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Hat tip to anyone whose code was used
* Inspiration
* etc
