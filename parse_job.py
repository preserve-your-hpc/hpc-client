import requests
import json
import re
import sys

import os.path

from random import randint

res_dict = {}

## run checkjob co
# bashCommand = "checkjob -v -v -v {}".format(sys.argv[1])
# import subprocess
# process = subprocess.Popen(bashCommand.split(), stdout=subprocess.PIPE)
# output, error = process.communicate()
# print(output.split())
# input = output

with open('file') as file:
    input = file.read()
    file.close()

## parse otput from checkjob command

res_dict['data'] = {}
res_dict['data']['memory'] = re.findall(r'Memory >=[ ]*([\s\S]*?) ', input)[0]
res_dict['data']['disk'] = re.findall(r'Disk >=[ ]*([\s\S]*?) ', input)[0]
res_dict['data']['swap'] = re.findall(r'Swap >=[ ]*([\s\S]*?)\n', input)[0]

res_dict['data']['avail_memory'] = re.findall(r'Available Memory >=[ ]*([\s\S]*?) ', input)[0]
res_dict['data']['avail_swap'] = re.findall(r'Available Swap >=[ ]*([\s\S]*?)\n', input)[0]

res_dict['data']['task_per_node'] = re.findall(r'TasksPerNode:[ ]*([\s\S]*?) ', input)[0]
res_dict['data']['node_count'] = re.findall(r'NodeCount:[ ]*([\s\S]*?)\n', input)[0]

modules = []
modules += re.findall(r'module load[\s]?(.*)\n', input)
modules += re.findall(r'#MSUB -v MPI_MODULE=[\s]?(.*)\n', input)
modules = list(set(modules))
# print(res_dict['modules'])
res_dict['script'] = re.findall(r'Job Submission\n[-]+\s(.*)', input, re.M|re.DOTALL)[0].strip('\n')


comstr = re.findall(r'\nmpirun[ ].*', input)[0]

res_dict['command'] = comstr.strip('\n')

files = {}
extra_files = re.findall(r'EXTRAFILES\=\"[\s]?(.*)\"', input)
for el in extra_files[0].split(' '):
    print(el)
    if el != '' and os.path.isfile(el):
        files[el] = open(el, 'rb')

input_files = re.findall(r'INFILE\=[\s]?(.*)', input)
for el in input_files[0].split(' '):
    print(el)
    if el != '' and os.path.isfile(el):
        files[el] = open(el, 'rb')

## send to API server

url = 'http://134.60.152.82/restapi/save-job/'
headers = {'Content-type': 'multipart/form-data'}
data = {"data": json.dumps(res_dict), "job_id": sys.argv[1], "modules":json.dumps(modules)}
# files = {'file': open('file.txt', 'rb')}
r = requests.post(url, files=files, data=data)
print(r.text)
